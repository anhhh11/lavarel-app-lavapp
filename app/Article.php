<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Services\NeedWritingLog;

class Article extends Model
{
    //TODO: add image to Article
    use SoftDeletes, NeedWritingLog;
    //
    protected $fillable = [
        'title',
        'body',
        'published_at',
        'user_id'
    ];
    //set<Column>Attribute
    public function setPublishedAtAttribute($date){
        $this->attributes['published_at'] = Carbon::parse($date);
    }
    public function getPublishedAtAttribute($date){
        return Carbon::parse($date)->format('Y-m-d');
    }
    public function scopePublished($query){
        $query->where('published_at','<=',Carbon::now());
    }
    public function scopeUnpublished($query){
        $query->where('published_at','>',Carbon::now());
    }

    /**
     * A article belongs to an article
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner(){
        return $this->belongsTo('App\User','user_id');
    }
    public function tags(){
        return $this->belongsToMany('App\Tag')->withTimestamps();
    }
    public function getTagListAttribute(){
        return $this->tags->lists('id')->toArray();
    }
}
