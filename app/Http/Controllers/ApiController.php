<?php

namespace App\Http\Controllers;

use App\Services\OAuth2;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    //
    private $user;
    private $oauth;

    public function __construct(OAuth2 $oauth)
    {
        $this->oauth = $oauth;
        $this->user = auth()->user();
    }

    public function index(){
        $clients = auth()
            ->user()
            ->clients()
            ->with('endpoint')
            ->latest()
            ->get();
        return view('api.index',['clients' => $clients]);
    }
    public function createClient(Request $req){
        $this->validate($req,[
            'id' => 'required|unique:oauth_clients,id',
            'redirect_uri' => 'required|url'
        ]);
        $this->oauth->createClient($this->user->username,$req->input('id'),$req->input('redirect_uri'));
        return redirect(action('ApiController@index'));
    }
}
