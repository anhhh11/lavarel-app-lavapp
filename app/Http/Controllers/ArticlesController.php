<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Article;
use App\Policies\ArticlePolicy;
use Carbon\Carbon;
use Gate;
use Illuminate\Http\Request;
use \Auth;
use App\Tag;
use App\Services\Calculator;
use Illuminate\Support\Str;


class ArticlesController extends Controller
{
    //
    public function __construct()
    {
        //except, only
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    public function index(Request $request)
    {
//        \Debugbar::disable();
//        \Debugbar::startMeasure('render','Index render');
        $orderBy = $request->input('order_by', 'title');
        $orderDir = $request->input('order_dir', 'desc');
        $articles = Article::with('owner')
            ->where('title', 'LIKE', '%' . $request->input('title') . '%')
            ->orderBy($orderBy, $orderDir)
            ->paginate(5);
//        \Debugbar::info($articles);
        return view('articles.index', [
            'articles' => $articles,
            'list_order' => [
                'order_by' => $orderBy,
                'order_dir' => $orderDir
            ]
        ]);
//        \Debugbar::stopMeasure('render');
    }

    public function show(Article $article, Calculator $calculator)
    {
//        $article = Article::findOrFail($id);
//        dd($article->created_at->addDays(0)->format('Y-m-d'));
        $truth = $calculator->add(1, 1) == 2;
        assert($truth, true);
        return view('articles.show', compact('article', 'truth'));
    }

    public function create()
    {
        return view('articles.create', ['allTags' => Tag::lists('name', 'id')]);
    }

    //TODO: move to Tag, use transaction
    private function createTagsFromTitle($title){
        $tags = array_map(function($name){
            //TODO: use whereIn for better performance
            $foundTag = Tag::where('name','LIKE',Str::lower($name))->first();
            if(!is_null($foundTag)) {
                $foundTagId = $foundTag['id'];
            } else {
                //TODO: use batch saving for better performance
                $newTag = Tag::create(['name' => Str::lower($name)]);
                $foundTagId = $newTag['id'];
            }
            return $foundTagId;
        },explode(' ',$title));
        return !is_null($tags) ? $tags : [];
    }
    public function store(Requests\ArticleRequest $request)
    {
        if (Auth::guest()) {
            return redirect('articles');
        }
        $input = $request->all();
//        $input['published_at'] = Carbon::now();
        $article = new Article($input);
        //$article->tags()->attach($request->tags);
        \Auth::user()->articles()->save($article);
        $tags = $this->createTagsFromTitle($input['title']);
        $article->tags()->sync($request->input('tag_list', $tags));
//        session()->flash('flash_message', "Created article $article->title");
//        session()->flash('flash_message_important', true);
//        flash()->overlay("Created article $article->title!",'Good job')->important();
        flash()->success("Created article $article->title!");
        return redirect('/articles');
    }

    public function edit(Article $article)
    {
        //Default ACL
//        $article = Article::findOrFail($id);
//        dd(Gate::denies('edit-or-delete-post',$article));
//        dd(Gate::allows('edit-or-delete-post',$article));
//        dd(auth()->user()->can('edit-or-delete-post',$article));
        //        $this->authorize(ArticlePolicy::EDIT_OR_DELETE_ARTICLE,$article);
        //Using Role plugin
        //->is('admin|mod') // is role
        //->can('edit.articles') // has permission
        //->allowed('edit.articles') // is owner or has the permission
        //->level()>=1 // check by level
        if(!auth()->user()->allowed('edit.articles',$article)){
            abort(403);
        }
        $allTags = Tag::lists('name', 'id');
        return view('articles.edit', compact('article', 'allTags'));
    }

    public function update(Article $article, Requests\ArticleRequest $request)
    {
        if(!auth()->user()->allowed('edit.articles',$article)){
            abort(403);
        }
//        $this->authorize(ArticlePolicy::EDIT_OR_DELETE_ARTICLE,$article);
        $article->update($request->all());
        $tags = $this->createTagsFromTitle($request->input('title'));
        $article->tags()->sync($request->input('tag_list', $tags));
        flash()->success("Updated article $article->title!");
        return redirect(action('ArticlesController@show', $article->id));
    }

    public function logout()
    {
        \Auth::logout();
        return redirect('home');
    }

    public function destroy(Article $article)
    {
        //Only admin or owner can delete
        if(!auth()->user()->can('delete.articles',$article)){
            abort(403);
        }
//        $this->authorize(ArticlePolicy::EDIT_OR_DELETE_ARTICLE,$article);
        $article->delete();
        flash()->success("Deleted article title $article->title");
        return redirect(action('ArticlesController@index'));
    }
}