<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Cookie;

class DemoOAuth2Client extends Controller
{
    private $GET_ACCESS_TOKEN_URI;
    private $CLIENT_ID;
    private $GRANT_TYPE_AUTHORIZATION_CODE;
    private $CLIENT_SECRET;
    private $REDIRECT_URI;

    private $config;
    const GRANT_TYPE_AUTHORIZATION_CODE = 'authorization_code';
    const GRANT_TYPE_CLIENT_CREDENTIALS = 'client_credentials';
    const GRANT_TYPE_PASSWORD = 'password';
    const GRANT_TYPE_WITH_REFRESH_TOKEN = 'refresh_token';
    private $client;

    public function __construct()
    {
        $this->config = [
            'server_base' => url(''),
            'get_access_token_uri' => url('oauth/access_token'),
            'client_id' => env('LAVAPP_API_SERVER_TEST_CLIENT_ID'),
            'client_secret' => env('LAVAPP_API_SERVER_TEST_CLIENT_SECRET'),
            'redirect_uri' => action('DemoOAuth2Client@getTokenFromCode')
        ];
        $this->client = new Client([
            'base_uri' => $this->config['server_base'],
            'http_errors' => false]);
    }

    public function getTokenFromCode(Request $request)
    {
        dump($this->config);
        $this->validate($request, [
            'code' => 'required'
        ]);
        $code = $request->input('code');

        $data = [
            'grant_type' => self::GRANT_TYPE_AUTHORIZATION_CODE,
            'client_id' => $this->config['client_id'],
            'client_secret' => $this->config['client_secret'],
            'redirect_uri' => $this->config['redirect_uri'],
            'code' => $code
        ];
        $res = $this->client->request('POST',
                                        $this->config['get_access_token_uri'],
                                        ['form_params' => $data]);
        return $res->getBody();
    }

    public function getTokenByClientCredential()
    {
        $data = [
            'grant_type' => self::GRANT_TYPE_CLIENT_CREDENTIALS,
            'client_id' => $this->config['client_id'],
            'client_secret' => $this->config['client_secret']
        ];
        $res = $this->client->request('POST',
                                $this->config['get_access_token_uri'],
                                ['form_params' => $data]);
        return $res->getBody();
    }
    public function getTokenByPassword(){
        $data = [
            'client_id' => $this->config['client_id'],
            'client_secret' => $this->config['client_secret'],
            'grant_type' => self::GRANT_TYPE_PASSWORD,
            'username' => 'admin',
            'password' => 'admin'
        ];
        $res = $this->client->request('POST',
            $this->config['get_access_token_uri'],
            ['form_params' => $data]);
        return $res->getBody();
    }
    public function getNewTokenFromRefreshToken($refreshToken)
    {
        $data = [
            'grant_type' => self::GRANT_TYPE_WITH_REFRESH_TOKEN,
            'client_id' => $this->config['client_id'],
            'client_secret' => $this->config['client_secret'],
            'refresh_token' => $refreshToken
        ];
        $res = $this->client->request('POST',
            $this->config['get_access_token_uri'],
            ['form_params' => $data]);
        return $res->getBody();
    }
}
