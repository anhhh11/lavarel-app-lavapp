<?php

namespace App\Http\Middleware;

use Closure;

class Demo
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->has('enterPrivateArea')
            && $request->enterPrivateArea == 1234
            && $request->is('articles/create')){
            return redirect('secret');
        }
        return $next($request);
    }
}
