<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
// Add your routes here
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //OAuth
    Route::get('oauth/authorize', [
        'as' => 'oauth.authorize.get',
        'middleware' => ['check-authorization-params', 'auth'], function () {
            $authParams = Authorizer::getAuthCodeRequestParams();

            $formParams = array_except($authParams, 'client');

            $formParams['client_id'] = $authParams['client']->getId();

            $formParams['scope'] = implode(config('oauth2.scope_delimiter'), array_map(function ($scope) {
                return $scope->getId();
            }, $authParams['scopes']));

            return View::make('oauth.authorization-form', ['params' => $formParams, 'client' => $authParams['client']]);
        }]);
    Route::post('oauth/authorize', [
        'as' => 'oauth.authorize.post',
        'middleware' => ['csrf', 'check-authorization-params', 'auth'], function () {
            $params = Authorizer::getAuthCodeRequestParams();
            $params['user_id'] = Auth::user()->id;
            $redirectUri = '/';

            // If the user has allowed the client to access its data, redirect back to the client with an auth code.
            if (Request::has('approve')) {
                $redirectUri = Authorizer::issueAuthCode('user', $params['user_id'], $params);
            }

            // If the user has denied the client to access its data, redirect back to the client with an error message.
            if (Request::has('deny')) {
                $redirectUri = Authorizer::authCodeRequestDeniedRedirectUri();
            }

            return Redirect::to($redirectUri);
        }]);
    Route::post('oauth/access_token', function () {
        return Response::json(Authorizer::issueAccessToken());
    });
    Route::group(['middleware' => ['csrf']], function () {
        Route::post('api/client/create', 'ApiController@createClient');
    });
    Route::group(['middleware' => 'auth'], function () {
        Route::get('api/', 'ApiController@index');
    });
    //End OAuth

    //Static pages
    Route::get('/contact', 'Welcome@contact');
    Route::get('/index', 'Welcome@index');
    Route::get('/about', 'PagesController@about');
    //End static pages

    //Auth
    Route::group(['middleware' => ['csrf']], function () {
        Route::controllers([
            'auth' => 'Auth\AuthController',
            'password' => 'Auth\PasswordController'
        ]);
    });
    //End auth

    //Article
    Route::group(['middleware' => ['csrf']], function () {
        Route::resource('articles', 'ArticlesController');
    });
    Route::get('logout', 'ArticlesController@logout');
    Route::get('/home', 'ArticlesController@index');
    Route::get('/', 'ArticlesController@index');
    Route::get('/secret', ['middleware' => ['auth', 'manager'], function () {
        return 'ABCD1234';
    }]);
    //End article


});

//Demo OAuth client
Route::group(['prefix' => 'demo-oauth2-client'],function(){
    Route::get('get-token-from-code','DemoOAuth2Client@getTokenFromCode');
    Route::get('get-token-by-client-credential','DemoOAuth2Client@getTokenByClientCredential');
    Route::get('get-token-by-password','DemoOAuth2Client@getTokenByPassword');
    Route::get('get-new-token-from-refresh-token/{refreshToken}','DemoOAuth2Client@getNewTokenFromRefreshToken');
});
//End demo OAuth client
//    Route::get('/articles','ArticlesController@index');
//    Route::get('/articles/create','ArticlesController@create');
//    Route::get('/articles/{id}','ArticlesController@show');
//    Route::post('/articles','ArticlesController@store');
//    Route::get('/articles/{id}/edit','ArticlesController@edit');