<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    //
    public $fillable = ['action','message','ip','user_id'];
}
