<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OAuthClient extends Model
{
    //
    public $table = 'oauth_clients';
    public $incrementing = false;
    public function endpoint(){
        return $this->hasOne('App\OAuthClientEndpoint','client_id','id');
    }
}
