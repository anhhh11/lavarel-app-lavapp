<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OAuthClientEndpoint extends Model
{
    public $table = 'oauth_client_endpoints';
    public $fillable = ['client_id','redirect_uri'];
}
