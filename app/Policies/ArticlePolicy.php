<?php

namespace App\Policies;

use App\Article;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ArticlePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    const EDIT_OR_DELETE_ARTICLE = 'editOrDeleteArticle';
    public function editOrDeleteArticle(User $user, Article $article){
        return $article->user_id == $user->id;
    }
}
