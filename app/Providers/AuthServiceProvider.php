<?php

namespace App\Providers;

use App\Article;
use App\User;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        \App\Article::class => \App\Policies\ArticlePolicy::class
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $gate->before(function ($user,$ability){
            if($user->username === 'admin'){
                return true;
            }
        });
        $this->registerPolicies($gate);
//        $gate->define('edit-or-delete-article',function(User $user,Article $article){
//            return $article->user_id == $user->id;
//        });
        //
    }
}
