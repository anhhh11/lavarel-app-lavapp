<?php
/**
 * Created by PhpStorm.
 * User: AnhHH_PHP
 * Date: 12/25/2015
 * Time: 4:18 PM
 */

namespace App\Services;

/**
 * Class Calculator
 * @package App\Services
 * Demo service injection
 */
class Calculator
{
    /**
     * Adding to number
     * @param int $a
     * @param int $b
     * @return int
     */
    public function add($a,$b){
        return $a  + $b;
    }
}