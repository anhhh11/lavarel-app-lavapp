<?php
/**
 * Created by PhpStorm.
 * User: anhhh11
 * Date: 12/26/2015
 * Time: 3:36 PM
 */
namespace App\Services;
use App\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

trait NeedWritingLog {
    /**
     * Write log
     * @param $action Action
     * @param $message Message
     * @return array Log entry
     */
    public static function log($action, $message){
        if(Auth::check()){
            $userId= auth()->user()->id;
        } else {
            $userId = null;
        }
        $logRow = [
            'ip' => Request::getClientIp(),
            'action' => $action . ' in ' . static::class,
            'message' => $message,
            'user_id' => $userId
        ];
        Log::create($logRow);
        return $logRow;
    }
    public static function boot(){
        parent::boot();
        static::creating(function($model){
            static::log('creating',json_encode($model));
        });
        static::created(function($model){
            static::log('created',json_encode($model));
        });
        static::updating(function($model){
            static::log('updating',json_encode($model));
        });
        static::updated(function($model){
            static::log('updated',json_encode($model));
        });
//        static::saving(function($model){
//            static::log('saving',json_encode($model));
//        });
//        static::saved(function($model){
//            static::log('saved',json_encode($model));
//        });
        static::deleting(function($model){
            static::log('deleting',json_encode($model));
        });
        static::deleted(function($model){
            static::log('deleted',json_encode($model));
        });
        static::restoring(function($model){
            static::log('restoring',json_encode($model));
        });
        static::restored(function($model){
            static::log('restored',json_encode($model));
        });

    }
}