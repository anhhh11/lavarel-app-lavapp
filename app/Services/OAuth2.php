<?php
/**
 * Created by PhpStorm.
 * User: anhhh11
 * Date: 12/27/2015
 * Time: 8:12 AM
 */

namespace App\Services;

use App\OAuthClientEndpoint;
use Illuminate\Support\Facades\DB;
use LucaDegasperi\OAuth2Server\Storage\FluentClient;
use Mockery\CountValidator\Exception;

class OAuth2
{
    public function __construct(FluentClient $client)
    {
        $this->client = $client;
    }

    /**
     * Create client based on username
     * @param $username Username
     * @param null $customId Optional - for testing purpose
     * @param string $redirectUri - Redirect url to receive code: <redirect uri>?code=someRandomCode
     */
    public function createClient($username, $customId = null, $redirectUri)
    {
        $id = is_null($customId) ? str_random(40) : $customId;
        $secret = str_random(40);
        DB::transaction(function() use($username,$id,$secret,$redirectUri){
            $this->client->create($username, $id, $secret);
            OAuthClientEndpoint::create([
                'client_id' => $id,
                'redirect_uri' => $redirectUri
            ]);
        });
        return [
            'id' => $id,
            'secret_key' => $secret,
            'name' => $username,
            'redirect_uri' => $redirectUri
        ];
    }

    public function get($clientId)
    {
        return $this->client->get($clientId);
    }
}