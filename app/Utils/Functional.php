<?php
/**
 * Created by PhpStorm.
 * User: anhhh11
 * Date: 12/26/2015
 * Time: 5:33 PM
 */
namespace App\Utils;
class Functional {
    public static function map(callable $func,$array){
        return array_map($func,$array);
    }
    // reduce: (acc,item)->acc initialAcc [item]
    public static function reduce(callable $reducer,$intialAcc,$items){
        return array_reduce($items,$intialAcc,$items);
    }
    public static function filter(callable $predicate, $items){
        return array_filter($items,$predicate);
    }
    public static function flatten($items){
        return array_flatten($items);
    }
    public static function each(callable $predicate, $items){
        array_walk($items,$predicate);
    }
}