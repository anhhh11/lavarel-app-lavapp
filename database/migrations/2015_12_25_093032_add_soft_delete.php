<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSoftDelete extends Migration
{
    private $ListTableAddingSoftDelte = ['articles', 'tags', 'users'];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        array_map(function($tableName){
            Schema::table($tableName, function($table)
            {
                $table->softDeletes();
            });
        }, $this->ListTableAddingSoftDelte);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        array_map(function($tableName){
            Schema::table($tableName, function($table)
            {
                $table->dropColumn('deleted_at');
            });
        }, $this->ListTableAddingSoftDelte);
    }
}
