<?php

use App\User;
use Illuminate\Database\Seeder;

class DeploySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //Create admin
        $oldAdmin = User::where('username','=','admin')->first();
        if($oldAdmin){
            $oldAdmin->delete();
        }
        $adminPassword = bcrypt("admin");
        $admin = User::create([
            'username' => "admin",
            'email' => "admin@dev.dev",
            'password' => $adminPassword,
            'remember_token' => str_random(10),
        ]);
        //Create role & permission
        $this->call(RolePermissionSeeder::class);
    }
}
