<?php

use App\User;
use Bican\Roles\Models\Permission;
use Illuminate\Database\Seeder;
use App\Utils\Functional as R;
use Bican\Roles\Models\Role;
class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Reset
        DB::table('permission_role')->delete();
        DB::table('permission_user')->delete();
        DB::table('role_user')->delete();
        DB::table('roles')->delete();
        DB::table('permissions')->delete();
//
        //Roles
        $adminRole = Role::create([
            'name' => 'Admin',
            'slug' => 'system.admin',
            'description' => 'Who having full control of system',
            'level' => 2
        ]);
        $modRole = Role::create([
            'name' => 'Moderate',
            'slug' => 'system.mod',
            'level' => 1
        ]);
        //Permissions
        $editArticlePermission = Permission::create([
            'name' => 'Edit articles',
            'slug' => 'edit.articles',
            'model' => 'App\Article'
        ]);
        $deleteArticlePermission = Permission::create([
            'name' => 'Delete articles',
            'slug' => 'delete.articles',
            'model' => 'App\Article'
        ]);
        //Roles' permissons
        //$adminRole->detachAllPermissions();
        $adminRole->attachPermission($editArticlePermission);
        $adminRole->attachPermission($deleteArticlePermission);
        $modRole->attachPermission($editArticlePermission);
        //Attach roles -> users
        //$admin->detachAllRoles();
        $admin = User::where('username','=','admin')->first();
        $admin->attachRole($adminRole);
        $mods = User::where('username','LIKE','mod%')->get();
        R::map(function($mod) use ($modRole){
            $mod->attachRole($modRole);
        },iterator_to_array($mods));
    }
}
