<?php

use Illuminate\Database\Seeder;
use App\Utils\Functional as R;
use App\User;
use App\Article;
use App\Tag;
class TagArticleUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //Not logging when seeding
        Article::unsetEventDispatcher();
        DB::table('logs')->delete();
        DB::table('tags')->delete();
        DB::table('articles')->delete();
        DB::table('users')->delete();
        $faker = Faker\Factory::create();
        //Create admin
        $adminPassword = bcrypt("admin");
        $admin = User::create([
            'username' => "admin",
            'email' => "admin@dev.dev",
            'password' => $adminPassword,
            'remember_token' => str_random(10),
        ]);
        //Create moderate
        $numberOfMod = 3;
        $modPassword = bcrypt("mod");
        $mods = R::map(function($i) use($modPassword){
            return User::create([
                'username' => "mod" . $i,
                'email' => "mod{$i}@dev.dev",
                'password' => $modPassword,
                'remember_token' => str_random(10),
            ]);
        },range(1,$numberOfMod));
        //Create random users
        $numberOfNormalUser = 10;
        $userPassword = bcrypt("user");
        $normalUsers = R::map(function($i) use ($faker,$userPassword) {
            return User::create([
                'username' => $faker->userName,
                'email' => $faker->email,
                'password' => $userPassword,
                'remember_token' => str_random(10)
            ]);
        },range(1,$numberOfNormalUser));
        //Users
        $users = array_merge([$admin],$mods,$normalUsers);
        //Create 0->x articles per user
        $maxNumberOfArticlePerUser = 10;
        $articles = array_flatten(R::map(function($user) use($faker,$maxNumberOfArticlePerUser){
            $numberOfArticleOfUser = rand(0,$maxNumberOfArticlePerUser);
            if($numberOfArticleOfUser > 0) {
                $userArticles = R::map(function ($i) use ($faker, $user) {
                    return \App\Article::create([
                        'title' => $faker->sentence(7, true),
                        'body' => $faker->paragraph(10, true),
                        'excerpt' => $faker->paragraph(2, true),
                        'published_at' => $faker->dateTimeBetween('-3 months', '+ 3 months')->format('Y-m-d'),
                        'user_id' => $user->id
                    ]);
                }, range(1, $numberOfArticleOfUser));
            }
            return !isset($userArticles) ? [] : $userArticles;
        },$users));
        //Create tags
        $numberOfTag = 30;
        $tags = R::map(function($i) use($faker){
            $createdTag = Tag::create([
                'name' => $faker->word
            ]);
            return $createdTag->id;
        },range(1,30));
        //Assign 0-10 tag to article
        R::each(function($article) use ($numberOfTag,$tags){
            shuffle($tags);
            $article->tags()->attach(array_slice($tags,0,rand(0,$numberOfTag)));
        },$articles);
    }
}
