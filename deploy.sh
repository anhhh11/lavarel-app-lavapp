#!/bin/bash
#   PS1="\u@\h:\w\n$ "
deploy(){
    # install all dependent libraries
    composer install

    # generate key for app
    php artisan key:generate

    # create db
    php artisan migrate

    # create testing db
    # php artisan migrate:refresh --database=mysql_testing
    # run test
    # ./vendor/bin/phpunit

    # seed data
    php artisan db:seed --class=DeploySeeder

    # install JS & CSS libraries
    cd ./public && bower install && cd ../

    # install dev tool
    # npm install
}

run(){
    echo 'Notice you must fill config file .env before running this'
    read -r -p "Are you config this file? [y/N] " response
    if [[ $response =~ ^([yY][eE][sS]|[yY])$ ]]; then
        deploy;
    else
        echo 'Please config .env file and return again. Thank.';
    fi
}

run;