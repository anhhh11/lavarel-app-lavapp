/**
 * Created by AnhHH_PHP on 12/25/2015.
 */

$(function () {
    $('.alert')
        .not('.alert-danger')
        .delay(3000)
        .slideUp();
    $('#flash-overlay-modal').modal();
});


$(function () {
    $('#article-task-list').select2({
        placeholder: 'Please select tag!',
        allowClear: true
    });
});


$(function () {
    $('.article .article-body').hide();
    $('.article').hover(function () {
        var $this = $(this);
        var t = setTimeout(function () {
            $this.find('.article-body').removeClass('hidden');
            $this.find('.article-body').slideDown(500);
        }, 1000);
        $this.data('timeout', t);
    }, function () {
        var $this = $(this);
        clearTimeout($this.data('timeout'));
        $this.find('.article-body').slideUp(500);
    });
});

$(function(){
    $('.btn-delete').on('click',function(){
        var $self = $(this);
        swal({
                title: "Are you sure?",
                text: "You will not be able to recover this record!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: true
            },
            function(){
                $self.closest('form').submit();
                //swal("Deleted!", "Your imaginary file has been deleted.", "success");
            });
        return false;
    });
});