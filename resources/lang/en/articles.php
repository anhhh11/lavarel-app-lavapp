<?php

return [
    'name' => 'Name',
    'title' => 'Title',
    'body' => 'Body',
    'create' => 'Create article',
    'published_at' => 'Published at',
    'edit' => 'Edit',
    'tags' => 'Tags',
    'search' => 'Search',
    'excerpt' => 'Excerpt'
];
