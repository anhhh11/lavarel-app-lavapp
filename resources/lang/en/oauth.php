<?php

return [
    'clientName' => 'Client name',
    'create' => 'Create',
    'redirect_uri' => 'Redirect URI',
    'approve' => 'Approve',
    'deny' => 'Deny'
];
