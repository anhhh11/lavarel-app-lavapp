@extends('app',['title' => 'API'])
@section('content')
    <ul class="nav nav-tabs">
        <li><a data-toggle="tab" href="#api-list">API list</a></li>
        <li class="active"><a data-toggle="tab" href="#applications">Clients (applications)</a></li>
    </ul>
    @include('errors.list');
    <div class="tab-content">
        <div id="api-list" class="tab-pane fade in">
        </div>
        <div id="applications" class="tab-pane fade in active">
            {!! Form::open(['action' => 'ApiController@createClient','method' => 'POST']) !!}
                    <!-- body ID Input -->
            <div class="form-group">
                {!! Form::label('client name (id)', trans('oauth.clientName'), ['class' => 'control-label']) !!}
                {!! Form::text('id', null, ['class' => 'form-control']) !!}
            </div>
            <!-- redirect URI Input -->
            <div class="form-group">
                {!! Form::label('redirect uri', trans('oauth.redirect_uri'), ['class' => 'control-label']) !!}
                {!! Form::text('redirect_uri', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">{{trans('oauth.create')}}</button>
            </div>
            {!! Form::close() !!}
            @foreach($clients as $client)
                {{--TODO: add click to copy--}}
                <dl>
                    <dt>ID (Client name)</dt>
                    <dd class="label label-warning">{{$client->id}}</dd>
                </dl>
                <dl>
                    <dt>Secret key</dt>
                    <dd class="label label-danger">{{$client->secret}}</dd>
                </dl>
                <dl>
                    <dt>Redirect URI</dt>
                    <dd class="label label-danger">{{$client->endpoint->redirect_uri}}</dd>
                </dl>
                <dl>
                    <dt>Get code link</dt>
                    <dd>
                        <?php $link = route('oauth.authorize.get', [
                                'client_id' => $client->id,
                                'redirect_uri' => $client->endpoint->redirect_uri,
                                'response_type' => 'code']); ?>
                        <a href="{{$link}}">{{$link}}</a>
                    </dd>
                </dl>
                <hr/>
            @endforeach
        </div>
    </div>
@stop