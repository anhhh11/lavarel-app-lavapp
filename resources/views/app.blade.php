<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{ $title }}</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="{{asset('bower_components/bootstrap/dist/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('bower_components/bootstrap/dist/css/bootstrap-theme.css')}}">

    <link rel="stylesheet" href="{{asset('bower_components/select2/dist/css/select2.css')}}">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('bower_components/sweetalert/dist/sweetalert.css')}}">
</head>
<body>
@include('partials.nav')
<div class='container'>
    @include('flash::message')
    @yield('content')
</div>
<hr />
<footer class="text-center">
    &copy; 2015
</footer>
<script src="{{asset('bower_components/jquery/dist/jquery.js')}}"></script>
<script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.js')}}"></script>
<script src="{{asset('bower_components/select2/dist/js/select2.js')}}"></script>
<script src="{{asset('bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
<script src="{{asset('js/app.js')}}"></script>
@yield('js')
</body>
</html>