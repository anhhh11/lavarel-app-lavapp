<!-- title Form Input -->
<div class="form-group">
    {!! Form::label('title', trans('articles.title'), ['class' => 'control-label']) !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>
<!-- body Form Input -->
<div class="form-group">
    {!! Form::label('body', trans('articles.body'), ['class' => 'control-label']) !!}
    {!! Form::textarea('body', null, ['class' => 'form-control']) !!}
</div>
<!-- body excerpt Input -->
<div class="form-group">
    {!! Form::label('excerpt', trans('articles.excerpt'), ['class' => 'control-label']) !!}
    {!! Form::textarea('excerpt', null, ['class' => 'form-control']) !!}
</div>
<!-- published_at Form Input -->
<div class="form-group">
    {!! Form::label('published_at', trans('articles.published_at'), ['class' => 'control-label']) !!}
    {!! Form::input('date','published_at', $article->published_at , ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('tag_list', trans('articles.tags'), ['class' => 'control-label']) !!}
    {!! Form::select('tag_list[]', $allTags , null , ['class' => 'form-control','multiple','id' => 'article-task-list']) !!}
</div>


<div class="form-group">
    {!! Form::submit($submitButton, ['class' => 'form-control btn btn-primary']) !!}
</div>