@extends('app',['title' => 'Create article'])
@section('content')
    <h1>Write a new article</h1>
    <hr/>

    {!! Form::model($article = new \App\Article(),['action' => 'ArticlesController@store']) !!}
        @include('articles._form',['submitButton' => 'Create'])
    {!! Form::close() !!}
    @include('errors.list')
@stop