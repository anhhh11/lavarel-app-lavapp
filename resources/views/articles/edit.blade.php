@extends('app',['title' => $article->title])
@section('content')
    @include('errors.list')
    <h1>Edit {{ $article->title  }}</h1>
    {!! Form::model($article,['method'=>'PATCH','action' => ['ArticlesController@update',$article->id]]) !!}
    @include('articles._form',['submitButton' => 'Update'])
    {!! Form::close() !!}
@stop