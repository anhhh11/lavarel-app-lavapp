@extends('app',['title' => 'List of articles'])
@section('content')
    <div class="row">
        <div class="col-md-12">
            {{--    {!! Form::open(['action'=>'ArticlesController@index','method' => 'get']) !!}--}}
            {!! Form::model($list_order,['action'=>'ArticlesController@index','method' => 'get']) !!}
            {!! Form::text('title', request()->input('title'), ['class' => 'form-control','placeholder' => 'Enter title here']) !!}
            <div class="input-group">
                <span class="input-group-btn">
                    <a class="btn">{{trans('common.orderByMessage')}}</a>
                </span>
                {!! Form::select('order_by', ['published_at'=>'published at','title' => 'title','created_at' => 'created at'],null, ['class' => 'form-control']) !!}
                <span class="input-group-btn">
                    <a class="btn">{{trans('common.byMessage')}}</a>
                </span>
                <span class="input-group-btn select-order-dir-wrapper">
                    {!! Form::select('order_dir', ['asc' => 'ascending','desc' => 'descending'],null, ['class' => 'form-control']) !!}
                </span>
                <span class="input-group-btn">
                    <button type="submit" class="form-control btn btn-primary">
                        <span class="glyphicon glyphicon-search"></span> {{trans('articles.search')}}
                    </button>
                </span>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="row above-normal-space">
        <div class="col-md-10">
            <span class="badge">
                There has {{$articles->total()}} articles (current {{$articles->currentPage()}}/{{$articles->lastPage()}} total)
            </span>
        </div>
        <div class="col-md-2">
            <a href="{{action('ArticlesController@create')}}" class="btn btn-success full-width-center">
                <span class="glyphicon glyphicon-file"></span> {{trans('articles.create')}}
            </a>
        </div>
    </div>
    @foreach($articles as $article)
        <article class="article">
            <!-- When new -->
            {{--{{action('ArticlesController@show',$article->id)}}--}}
                    <!-- When maintaining -->
            {{--{{url('/articles',$article->id)}}--}}
            <a href="{{action('ArticlesController@show',$article->id)}}">
                <h3 class="display-inline">{{$article->title}}</h3>
            </a>
            <em>(published by {{$article->owner->username}} at {{$article->published_at}})</em>
            <div class="article-body hidden">
                {{$article->body}}
            </div>
            <div>
                {{--@can(\App\Policies\ArticlePolicy::EDIT_OR_DELETE_ARTICLE,$article)--}}
                {!! Form::open(['action' => ['ArticlesController@destroy',$article->id], 'method' => 'delete','class' => 'form-inline display-inline']) !!}
                @allowed('edit.articles',$article)
                    <a href="{{action('ArticlesController@edit',$article->id)}}"
                       class="btn btn-warning form-control">{{trans('articles.edit')}}</a>
                @endallowed
                @allowed('delete.articles',$article)
                    {!! Form::submit('Delete', ['class' => 'form-control btn btn-danger btn-delete']) !!}
                @endallowed
                {!! Form::close() !!}

                {{--@endcan--}}
            </div>
            <hr/>
        </article>
    @endforeach
    <div>
        {{--        {!! $articles->appends(['title' => request()->title])->links() !!}--}}
        {!! $articles->appends(request()->all())->links() !!}
    </div>
@stop