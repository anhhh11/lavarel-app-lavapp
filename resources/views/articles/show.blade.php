@extends('app',['title' => $article->title ])
@section('content')
    <h1>{{$article->title}}</h1>
    <p class="well">{{$article->excerpt}}</p>
    <p>
        {{$article->body}}
    </p>
    <hr />
    @unless($article->tags->isEmpty())
        <h2>Tags</h2>
        <ul class="list-inline">
            @foreach($article->tags as $tag)
                <li>
                    <span class="badge">
                        {{$tag->name}}
                    </span>
                </li>
            @endforeach
        </ul>
    @endunless
    <hr />
    <a href="{{action('ArticlesController@index',$article->id)}}" class="btn btn-default">{{trans('common.back')}}</a>
    {{--@can(\App\Policies\ArticlePolicy::EDIT_OR_DELETE_ARTICLE,$article)--}}
    @allowed('edit.articles',$article)
        <a href="{{action('ArticlesController@edit',$article->id)}}" class="btn btn-warning">{{trans('common.edit')}}</a>
    @endallowed
    {{--@endcan--}}
@stop