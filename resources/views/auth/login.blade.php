@extends('app',['title' => trans('Login')])
@section('content')
@include('errors.list')
{!! Form::open(['url' => 'auth/login', 'method' => 'post']) !!}
        <!-- username Form Input -->
<div class="form-group">
    {!! Form::label('username', trans('auth.username'), ['class' => 'control-label']) !!}
    {!! Form::text('username', null, ['class' => 'form-control']) !!}
</div>
<!-- password Form Input -->
<div class="form-group">
    {!! Form::label('password', trans('auth.password'), ['class' => 'control-label']) !!}
    {!! Form::input('password','password', null, ['class' => 'form-control']) !!}
</div>
<!-- rememberMe Form Input -->
<div class="form-group">
    <label>
        {!! Form::checkbox('remember', '1', null,  ['id' => 'auth-remember']) !!}
        {{trans('auth.remember')}}
    </label>
</div>
<div class="form-group">
    <label>
        <a href="{{action('Auth\PasswordController@getEmail')}}">I've forgotten my password</a
    </label>
</div>
{!! Form::submit('Login', ['class' => 'form-control btn btn-primary']) !!}
{!! Form::close() !!}
@stop