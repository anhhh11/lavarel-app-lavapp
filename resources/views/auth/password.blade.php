@extends('app',['title'=>'Reset password'])
@section('content')
<form method="POST" action="{{action('Auth\PasswordController@getEmail')}}">
    {!! csrf_field() !!}

    @include('errors.list')

    <div class="form-group">
        {!! Form::label('email', trans('auth.email'), ['class' => 'control-label']) !!}
        {!! Form::text('email', old('email'), ['class' => 'form-control']) !!}
    </div>

    <div>
        <button type="submit" class="btn btn-success">
            Send Password Reset Link
        </button>
    </div>
</form>
@stop