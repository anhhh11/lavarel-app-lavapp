@extends('app',['title' => trans('Register')])
@section('content')
{!! Form::open(['url' => 'auth/register', 'method' => 'post']) !!}
        <!-- username Form Input -->
<div class="form-group">
    {!! Form::label('username', trans('auth.username'), ['class' => 'control-label']) !!}
    {!! Form::text('username', null, ['class' => 'form-control']) !!}
</div>
<!-- email Form Input -->
<div class="form-group">
    {!! Form::label('email', trans('auth.email'), ['class' => 'control-label']) !!}
    {!! Form::input('email','email',null, ['class' => 'form-control']) !!}
</div>
<!-- password Form Input -->
<div class="form-group">
    {!! Form::label('password', trans('auth.password'), ['class' => 'control-label']) !!}
    {!! Form::input('password','password', null, ['class' => 'form-control']) !!}
</div>
<!-- password confirm Form Input -->
<div class="form-group">
    {!! Form::label('password_confirmation', trans('auth.passwordConfirmation'), ['class' => 'control-label']) !!}
    {!! Form::input('password','password_confirmation', null, ['class' => 'form-control']) !!}
</div>
{!! Form::submit('Register', ['class' => 'form-control btn btn-success']) !!}
@include('errors.list')
{!! Form::close() !!}
@stop
