@extends('app',['title'=>'Reset password'])
@section('content')
    <form method="POST" action="{{action('Auth\PasswordController@postReset')}}">
        {!! csrf_field() !!}
        <input type="hidden" name="token" value="{{ $token }}">
        @include('errors.list')

        <!-- email Form Input -->
        <div class="form-group">
            {!! Form::label('email', trans('auth.email'), ['class' => 'control-label']) !!}
            {!! Form::input('email','email',null, ['class' => 'form-control']) !!}
        </div>
        <!-- password Form Input -->
        <div class="form-group">
            {!! Form::label('password', trans('auth.password'), ['class' => 'control-label']) !!}
            {!! Form::input('password','password', null, ['class' => 'form-control']) !!}
        </div>
        <!-- password confirm Form Input -->
        <div class="form-group">
            {!! Form::label('password_confirmation', trans('auth.passwordConfirmation'), ['class' => 'control-label']) !!}
            {!! Form::input('password','password_confirmation', null, ['class' => 'form-control']) !!}
        </div>

        <div>
            <button type="submit" class="btn btn-success">
                Reset password
            </button>
        </div>
    </form>
@stop