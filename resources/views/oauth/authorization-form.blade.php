@extends('app',['title' => "Application {{$client->getId()}} want to access your account"])
@section('content')
    <h2>Application `{{$client->getId()}}` want to access your account ({{$client->getName()}})</h2>
    {{--TODO: with the following information--}}
    <form method="post" action="{{route('oauth.authorize.post', $params)}}">
        {{csrf_field()}}
        <input type="hidden" name="client_id" value="{{$params['client_id']}}">
        <input type="hidden" name="redirect_uri" value="{{$params['redirect_uri']}}">
        <input type="hidden" name="response_type" value="{{$params['response_type']}}">
        <input type="hidden" name="state" value="{{$params['state']}}">
        <input type="hidden" name="scope" value="{{$params['scope']}}">
        <button type="submit" name="approve" value="approve" class="btn btn-warning">
            <span class="glyphicon glyphicon-check"></span> {{trans('oauth.approve')}}
        </button>
        <button type="submit" name="deny" value="deny" class="btn btn-success">
            <span class="glyphicon glyphicon-exclamation-sign"></span> {{trans('oauth.deny')}}
        </button>
    </form>
@stop