<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{url('/')}}">Lavapp</a>
        </div>
        <div>
            <ul class="nav navbar-nav">
                <li><a href="{{url('/home')}}"><span class="glyphicon glyphicon-home"></span> Home</a></li>
                @foreach($top2Articles as $article)
                    <li class="bg-warning">
                        <a href="{{action('ArticlesController@show',$article->id)}}">
                            <span class="glyphicon glyphicon-paperclip"></span> {{$article->title}}
                        </a>
                    </li>
                @endforeach
            </ul>
            <ul class="nav navbar-nav navbar-right">
                @if(\Auth::check())
                    <li><a>Hello, {{\Auth::user()->username}}</a></li>
                    <li>
                        <a href="{{url('/api')}}"><span class="glyphicon glyphicon-globe"></span> API</a>
                    </li>
                    <li><a href="{{url('/logout')}}"><span
                                    class="glyphicon glyphicon-log-out"></span> {{trans('logout')}}</a></li>
                @else
                    <li><a href="{{url('/auth/login')}}"><span
                                    class="glyphicon glyphicon-log-in"></span> {{trans('auth.login')}}</a></li>
                    <li><a href="{{url('/auth/register')}}"><span
                                    class="glyphicon glyphicon-registration-mark"></span> {{trans('auth.register')}}</a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>