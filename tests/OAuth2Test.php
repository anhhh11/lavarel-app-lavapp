<?php

use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use \app\Services\OAuth2;


class OAuth2Test extends TestCase
{
    use DatabaseTransactions;
    public function testCreateClient()
    {
        //create user
        //TODO: refactor using model factory
        $user = User::create([
            'username' => str_random(30),
            'email' => str_random(30),
            'password' => ''
        ]);
        $id = str_random(40);
        $redirect_uri = 'http://dev.com';
        $username = $user->username;
        //create application
        $oauth2 = $this->app->make('App\Services\OAuth2');
        $oauth2->createClient($username,$id,$redirect_uri);
        //check if it is created
        $this->seeInDatabase('oauth_clients',['name' => $username]);
        $this->seeInDatabase('oauth_client_endpoints',[
            'client_id' => $id,
            'redirect_uri' => $redirect_uri
        ]);
    }
    public function testAuthorizeUsingCode(){
        //Create user
        //TODO: refactor using model factory
        $user = User::create([
            'username' => str_random(30),
            'email' => str_random(30),
            'password' => ''
        ]);
        //Login
        $this->be($user);
        //Create client(application)
        $oauth2 = $this->app->make('App\Services\OAuth2');
        $redirectUri = env('APP_URL') . 'demo-oauth2-client/get-token-from-code';
        $client = $oauth2->createClient($user->username,
                                        'some client 123456',
                                        $redirectUri);
        //Check if it is created
        $this->seeInDatabase('oauth_clients',['name' => $user->username]);
        $this->seeInDatabase('oauth_client_endpoints',
            [
                'client_id' => $client['id'],
                'redirect_uri' => $client['redirect_uri']
            ]);
        //Application asks for authorize
        $data = [
            'client_id' => $client['id'],
            'redirect_uri' => $client['redirect_uri'],
            'response_type' => 'code'
        ];
        $url = 'oauth/authorize' . '?' . http_build_query($data);
        //Show screen confirm to user
        $csrfToken = $this
            ->visit($url)
            ->getInputOrTextAreaValue('_token');
        //Give redirect url with code back
        $redirectBackUrl = $this->post($url . '&amp;scope=',array_merge([],$data,[
            '_token' => $csrfToken,
            'approve' => 'approve',
            'state' => '',
            'scope' => ''
        ]))->response->headers->get('location');
        //Get `code` from URL
        parse_str(parse_url($redirectBackUrl)['query'],$qr);
        $code = $qr['code'];
        $this->seeInDatabase('oauth_auth_codes',['id' => $code]);
        //Get token
        $result = json_decode($this->post('oauth/access_token',[
                'grant_type' => 'authorization_code',
                'client_id' => $client['id'],
                'client_secret' => $client['secret_key'],
                'redirect_uri' => $client['redirect_uri'],
                'code' => $code])->response->getContent(),true);
//        dump("Token got:",$result);
        $this->assertExistsToken($result);
    }
    public function testAuthorizeUsingClientCredential(){
        //Create user
        //TODO: refactor using model factory
        $user = User::create([
            'username' => str_random(30),
            'email' => str_random(30),
            'password' => ''
        ]);
        //Login
        $this->be($user);
        //Create client(application)
        $oauth2 = $this->app->make('App\Services\OAuth2');
        $redirectUri = env('APP_URL') . 'demo-oauth2-client/get-token-from-code';
        $client = $oauth2->createClient($user->username,
            'some client 123456',
            $redirectUri);
        //Get token
        $data = [
            'grant_type' => 'client_credentials',
            'client_id' => $client['id'],
            'client_secret' => $client['secret_key']
        ];
        dump('To run this test must change config `client_credentials.access_token_ttl` to 10');
        $token = json_decode($this->post('oauth/access_token',$data)->response->getContent(),true);
        $this->assertExistsToken($token);
        $access_token = $token['access_token'];
    }

    /**
     * @param $result
     */
    public function assertExistsToken($result)
    {
        $this->assertArrayHasKey('access_token', $result, 'Not found token');
        $this->assertArrayHasKey('token_type', $result, 'Not found token type');
        $this->assertEquals($result['token_type'], 'Bearer', 'Invalid token type, expect `Bearer`');
        $this->assertArrayHasKey('expires_in', $result, 'Not found expire in');
    }
}
